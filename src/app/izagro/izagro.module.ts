import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { izagroComponent } from './izagro/izagro.component';


@NgModule({
  declarations: [
    izagroComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [izagroComponent]
})
export class IzagroModule { }
