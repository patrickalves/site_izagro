import { Component } from '@angular/core';

@Component({
  selector: 'izagro-root',
  templateUrl: './izagro.component.html',
  styleUrls: ['./izagro.component.css']
})
export class izagroComponent {
  title = 'IZagro - A informação na sua mão';
}